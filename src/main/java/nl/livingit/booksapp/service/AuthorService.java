package nl.livingit.booksapp.service;

import nl.livingit.booksapp.model.Author;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface AuthorService {

    List<Author> findAll();
    List<Author> getAll();
    Optional<Author> findById(Long authorId);

    Author save(Author author);

    @Transactional
    void deleteById(Long authorId);

    List<Author> findByNameContainsIgnoreCase(String name);
}

