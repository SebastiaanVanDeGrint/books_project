package nl.livingit.booksapp.repository;

import nl.livingit.booksapp.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
    List<Author> findByNameContainsIgnoreCase(String nameContains);

}
