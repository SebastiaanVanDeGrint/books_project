package nl.livingit.booksapp.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

public class AuthorDto {
    private Long id;
    private String name;

    @JsonIgnoreProperties("authors")
    private List<BookDto> books = new ArrayList<>();

    public AuthorDto() {
    }

    public AuthorDto(Long id, String name, List<BookDto> books) {
        this.id = id;
        this.name = name;
        this.books = books;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BookDto> getBooks() {
        return books;
    }

    public void addBook(BookDto book) {
        books.add(book);
    }
}
