package nl.livingit.booksapp.objectMapper;

import nl.livingit.booksapp.model.Book;
import nl.livingit.booksapp.model.Owner;
import nl.livingit.booksapp.web.dto.BookDto;
import nl.livingit.booksapp.web.dto.OwnerDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OwnerMapper {
    public OwnerDto toDto(Owner owner){
        List<BookDto> books = new ArrayList<>();
        Long id = owner.getId();
        String name = owner.getName();
        for(Book book : owner.getBooks()){
            BookDto bookDto = new BookDto();
            bookDto.setId(book.getId());
            bookDto.setGenre(book.getGenre());
            bookDto.setIsbn(book.getIsbn());
            bookDto.setTitle(book.getTitle());
            bookDto.setPages(book.getPages());
            bookDto.setSummary(book.getSummary());
            books.add(bookDto);
        }
        return new OwnerDto(id, name, books);
    }

    public Owner toOwner(OwnerDto ownerDto){
        List<Book> books = new ArrayList<>();
        Long id = ownerDto.getId();
        String name = ownerDto.getName();
        for(BookDto bookDto : ownerDto.getBooks()){
            Book book = new Book();
            book.setId(bookDto.getId());
            book.setGenre(bookDto.getGenre());
            book.setIsbn(bookDto.getIsbn());
            book.setTitle(bookDto.getTitle());
            book.setPages(bookDto.getPages());
            book.setSummary(bookDto.getSummary());
            books.add(book);
        }
        return new Owner(id, name, books);
    }

}
