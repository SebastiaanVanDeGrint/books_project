package nl.livingit.booksapp.objectMapper;

import nl.livingit.booksapp.model.Book;
import nl.livingit.booksapp.model.Owner;
import nl.livingit.booksapp.model.Rating;
import nl.livingit.booksapp.web.dto.BookDto;
import nl.livingit.booksapp.web.dto.OwnerDto;
import nl.livingit.booksapp.web.dto.RatingDto;
import org.springframework.stereotype.Component;

@Component
public class RatingMapper {
    public RatingDto toDto(Rating rating) {
        Long id = rating.getId();
        Double bookRating = rating.getRating();
        BookDto bookDto = new BookDto();
        Book book = rating.getBook();
        bookDto.setId(book.getId());
        bookDto.setGenre(book.getGenre());
        bookDto.setIsbn(book.getIsbn());
        bookDto.setTitle(book.getTitle());
        bookDto.setPages(book.getPages());
        bookDto.setSummary(book.getSummary());
        OwnerDto ownerDto = new OwnerDto();
        Owner owner = rating.getOwner();
        ownerDto.setId(owner.getId());
        ownerDto.setName(owner.getName());

        return new RatingDto(id, bookRating, bookDto, ownerDto);
    }
    public Rating toRating(RatingDto ratingDto) {
        Long id = ratingDto.getId();
        Double bookRating = ratingDto.getRating();
        Book book = new Book();
        BookDto bookDto = ratingDto.getBook();
        book.setId(bookDto.getId());
        book.setGenre(bookDto.getGenre());
        book.setIsbn(bookDto.getIsbn());
        book.setTitle(bookDto.getTitle());
        book.setPages(bookDto.getPages());
        book.setSummary(bookDto.getSummary());
        Owner owner = new Owner();
        OwnerDto ownerDto = ratingDto.getOwner();
        owner.setId(ownerDto.getId());
        owner.setName(ownerDto.getName());

        return new Rating(id, bookRating, book, owner);
    }
}
