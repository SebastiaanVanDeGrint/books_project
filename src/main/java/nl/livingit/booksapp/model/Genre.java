package nl.livingit.booksapp.model;

public enum Genre {
    ACTION,
    DETECTIVE,
    COMIC,
    EDUCATIONAL,
    FANTASY,
    FICTION,
    SCIFI,
    THRILLER,
    BIOGRAPHY,
    COOKBOOK
}
